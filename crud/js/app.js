document.addEventListener("DOMContentLoaded", function() {

	const commentSource = 'http://jsonplaceholder.typicode.com/comments';
	const commentStatus = document.querySelector('.comments_status');
	const commentsContainer = document.querySelector('.comments_list');
	const pagination = document.querySelector('.pagination');
	const addForm = document.querySelector('.add_form');
	const limitForm = document.querySelector('.limit_form');
	const limitInput = limitForm.querySelector('#limit');

	addForm.addEventListener('submit', (e) => add(e));
	limitForm.addEventListener('submit', (e) => {
		e.preventDefault();
		renderPage(page);
	})

	let page = 1;

	function getComments(page){
	  	try {
	  		fetch(commentSource).then(function(response) {
	  		  return response.json();
	  		}).then(function(response) {
	  			let limit = limitInput.value;
	  		  	createPagination(response.length, limit);
	  		  	(page) ? paginate(response, page, limit) : showComments(response);
	  		});
	  	} catch(e) {
	  		commentStatus.textContent = e.message;
	  		console.error(e);
	  	}
	}

	function createPagination(items, limit){
		let pagiInsert = `<span class="page">1</span>`;
		for (let i = 2; i <= Math.ceil(items/limit); i++) {
			pagiInsert = pagiInsert + `<span class="page">${i}</span>`
		}
		pagination.insertAdjacentHTML('beforeend', pagiInsert)
		let pages = Array.from(pagination.querySelectorAll('.page'));
		pages.map((page) => {
			page.addEventListener('click', (e) => renderPage(e.target.textContent))
		})
	}

	function paginate(comments, page, limit){
		let paginated = comments.filter((comment) => {
			return (comment.id <= (page*limit)) && (comment.id > (page*limit - limit)) 
		})
		showComments(paginated);
	}

	function renderPage(pageNo){
		console.log(pageNo);
		commentsContainer.innerHTML = "";
		pagination.innerHTML = "";
		getComments(pageNo);
	}

	function showComments(comments){
		if(comments) commentStatus.textContent = '';
		comments.map((comment) => {
			commentsContainer.insertAdjacentHTML('beforeend', constructComment(comment))
		})
		setUpListeners();
	}

	function setUpListeners(){
		let editButtons = Array.from(document.querySelectorAll('.edit'));
		editButtons.map(function(button){
			button.addEventListener('click', (e) => edit(e))
		});

		let removeButtons = Array.from(document.querySelectorAll('.remove'));
		removeButtons.map(function(button){
			button.addEventListener('click', (e) => remove(e))
		});
	}

	function constructComment(data){
		return `<li class="comment_item">`
                +`<p class="comment_item--id">${data.id}</p>`
                +`<p><button class="comment_item--button edit">Edit</button>`
                +`<button class="comment_item--button remove">Remove</button></p>`
                +`<p class="comment_item--name">${data.name}</p>`
                +`<p class="comment_item--email">${data.email}</p>`
                +`<p class="comment_item--body">${data.body}</p>`
              +`</li>`
	}

	function add(e) {
		e.preventDefault();
		const data = {
			id : commentsContainer.childElementCount + 1,
			name : e.target[0].value,
			email : e.target[1].value,
			body : e.target[2].value
		}
		commentsContainer.insertAdjacentHTML('beforeend', constructComment(data));
		setUpListeners();	
	}

	function remove(e){
		const target = e.target.parentNode.parentNode;
		commentsContainer.removeChild(target);
	}

	function renderEditForm(data) {
		return `<form name="edit_comment" method="post" action="" class="edit_form">`
              +`<span class="input_text"><label for="name">Name</label><input type="text" name="name" id="name" max="20" min="5" required="" value="${data.name}"></span>`
              +`<span class="input_text"><label for="email">E-mail</label><input type="email" name="email" id="email" max="20" min="15" required="" value="${data.email}"></span>`
              +`<label for="body">Comment</label>`
              +`<textarea name="body" id="body" required class="input_textar">${data.body}</textarea>`
              +`<span class="input_button"><button type="reset">Reset</button><button type="submit">Add</button></span>`
            +`</form>`
	}
	function edit(e){
		const target = e.target.parentNode.parentNode;
		const data = {
			'name' : target.children[2].textContent,
			'email' : target.children[3].textContent,
			'body' : target.children[4].textContent
		}
		target.insertAdjacentHTML('beforeend', renderEditForm(data));
		target.addEventListener('submit', (e) => {
			e.preventDefault();
			const data = {
				id : target.children[0].textContent,
				name : e.target[0].value,
				email : e.target[1].value,
				body : e.target[2].value,
			}
			target.innerHTML = constructComment(data);
			setUpListeners();
		})
	}

	getComments(page);
})