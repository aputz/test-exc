var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var fileInclude = require('gulp-file-include');

gulp.task('sass', function() {
  return gulp.src('scss/style.scss')
    .pipe(sass({
      outputStyle: 'compressed', // if css compressed **file size**
      includePaths: require('node-normalize-scss').includePaths
    })
      .on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: ['last 2 versions', 'ie >= 9']
    }))
    .pipe(gulp.dest('build/css'))
});

gulp.task('fileInclude', function() {
  gulp.src('html/index.html')
  .pipe(fileInclude({
    prefix: '@@',
    basepath: '@file'
  }))
  .pipe(gulp.dest('build/'));
})


gulp.task('watch', ['sass'], function (){
  gulp.watch('scss/**/*.scss', ['sass']);
  gulp.watch('html/**/*.html', ['fileInclude']);
})

gulp.task('default', ['sass'], function() {
  gulp.watch(['scss/**/*.scss'], ['sass']);
  gulp.watch('html/**/*.html', ['fileInclude']);
});