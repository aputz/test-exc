# Odzorowanie strony
Strona na podstawie dostarczonego szablonu. Zawiera RWD i była tworzona z wykorzystaniem podejścia Mobile First. Nazewnictwo oparte na BEM. Potrzebne grafiki były kompresowane przy pomocy wbudowanego narzędzia Adobe Photoshop.

## Wykorzystane
* Gulp - kompilacja do html i css
* gulp-file-include - kompilacja HTML
* gulp-sass - kompilacja SCSS
* gulp-autoprefixer
* Grid Bootstrap - grid użyty jako baza layoutu
* Waypoints.js - animacja menu

## Szanse
Pewne elementy strony wymagają dopracowania, na co już niestety nie starczyło czasu. Z pewnością stronie brakuje interaktywności tam, gdzie byłaby oczekiwana (np. prezentacja członków zarządu), natomiast obecna struktura strony stanowi po to dobrą bazę. Także ostylowanie wymaga pewnej optymalizacji, gdyż zawartośc plików scss jest nieco redundantna.

# CRUD
CRUD to skrót od angielskich nazw podstawowych operacji na zbiorach danych.
Są to odpowienio :
* Create (tworzenie)
* Read (listowanie)
* Update (modyfikacja)
* Delete (usunięcie)

## Wykonanie zadania
Strona listuje komentarze pobrane przez Ajax, możliwe są też pozostałe operacje CRUD (przy czym, z oczywistych względów, jedynie lokalnie). Dodana też została paginacja, aczkolwiek działa ona chyba trochę inaczej, niż w przykładzie.

## Szanse
Nie ma też możliwości sortowania wyników. Strona wizualna mocno kuleje, co jest możliwe do poprawy w najbliższym czasie.