document.addEventListener("DOMContentLoaded", function() {

	(function menuHistory() {
		const navi = document.querySelector('.navigation_menu');

		navi.addEventListener('click', (e) => {
			e.preventDefault();
			console.log(e.target, e.target.tagName === 'A');
			if (e.target.tagName === "A") {
				let target = document.querySelector(`#${e.target.hash.slice(1)}`);
				history.pushState(null, null, e.target.hash);
				var pos = target.getBoundingClientRect().top;
				window.scroll({
				  top: pos - 64, 
				  left: 0, 
				  behavior: 'smooth' 
				});
			}
		})
	})();

	(function mamagementTabs() {
		const icons = Array.from(document.querySelectorAll('.management_item'));
		const tab = document.querySelector('.info');
		let tabName = tab.querySelector('.info_text--name');
		let tabPhoto = tab.querySelector('.info_photo img');
		icons.map((icon) => {
			icon.addEventListener('click', () => {
				let newName = icon.querySelector('img').alt;
				let newPhoto = icon.querySelector('img').src;
				tabName.textContent = newName;
				tabPhoto.src = newPhoto.slice(0, -4) + '_big' + newPhoto.slice(-4);
				tabPhoto.alt = newName + ' zdięcie';
				icons.map((icon) => icon.classList.remove('ac'));
				icon.classList.add('ac');
			})
		})
	})();

	(function commentsToggle() {
		const commentBody = document.querySelector('.comments_body');
		const commentSwitcher = document.querySelector('.comments_more');

		commentSwitcher.addEventListener('click', () => {
			commentBody.classList.toggle('inview');
		})
	})();

	var waypoint = new Waypoint({
	  element: document.querySelector('.navigation'),
	  handler: function() {
	  	var nav = this.element;
	    nav.classList.toggle('moving');
	  },
	  offset: '-70%'
	})

})